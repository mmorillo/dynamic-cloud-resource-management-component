#Dynamic Cloud Resource Management Component#

#Overview#

This is an initial release of the result developed during the writing of the final project Gestión dinámica de recursos en sistemas Cloud realized in the Escuela Superior de Ingeniería Informática (UCLM) of Albacete, developed by Miguel Morillo Iruela and conducted by María Blanca Caminero Herráez and María Carmen Carrión Espinosa.

The main objective of this project is the development and integration of a component that develops an algorithm for efficiently managing Cloud infrastructure based on open source (OpenNebula).

For this we have proposed a monitoring tool (Nagios) integrated with an ad-hoc developed component to provide an environment to manage virtual resources dynamically using Nagios, OpenNebula and the component DCRMC (Dynamic Cloud Resource Management Component).


# License

```
Copyright (c) 2012 UCLM, Miguel Morillo Iruela, María Blanca Caminero Herráez, María Carmen Carrión Espinosa
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. Neither the name of copyright holders nor the names of its
   contributors may be used to endorse or promote products derived
   from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
''AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL COPYRIGHT HOLDERS OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.
```


#TestBed

* OpenNebula 3.2.1
* NFS versión 1.2.4
* Nagios® Core™ 3.3.1
* DCRMC
* xen 3.1
* Nginx




# DCRMC Configuration

Component configuration example; config_one.xml:

```
#!xml

<?xml version='1.0'?>
<one>
        <ONE>http://192.168.10.40:2633/RPC2</ONE>
        <oneuser>oneadmin</oneuser>
        <onepass>oneadmin</onepass>
        <maxVM>3</maxVM>
        <templateVM>
          REQUIREMENTS = "HYPERVISOR=\"xen\""
          NAME   = centos_vm
          CPU    = 1
          MEMORY = 256
          DISK = [ image = "Centos_Nagios_nrpe",target = "xvda",readonly = "no" ]
          NIC    = [ NETWORK_ID = 1 ]
          OS = [ bootloader = "/usr/bin/pygrub" ]
          GRAPHICS = [ type    = "vnc", listen  = "127.0.0.1" ]
        </templateVM>
        <VM1>
                <Description>FRONTAL WEB</Description>
                <IP>192.168.10.30</IP>
                <STATE>FIXED</STATE>
        </VM1>
        <VM2>
                <Description>RESPALDO_1</Description>
                <IP>192.168.10.31</IP>
                <STATE>RESERVED</STATE>
        </VM2>
        <VM3>
                <Description>RESPALDO_2</Description>
                <IP>192.168.10.32</IP>
                <STATE>RESERVED</STATE>
        </VM3>
</one>
```

# DCRMC Installation

Copy the config file into nagios etc folder:

```
#!bash
cp config_one.xml /usr/local/nagios/etc/
```

Copy the script file into nagios libexec folder: 

```
#!bash
cp dcrmc_one.pl /usr/local/nagios/libexec/
```

Make sure that nagios user can execute the script file $USER1$/dcrmc_one.pl:

```
#!bash
chown nagios:nagios /usr/local/nagios/libexec/dcrmc_one.pl
chmod 0750 /usr/local/nagios/libexec/dcrmc_one.pl
```

Make sure that nagios user can write into log file:

```
#!bash
touch /usr/local/nagios/var/dcrmc_one.log
chown nagios:nagios /usr/local/nagios/var/dcrmc_one.log
chmod 0644 /usr/local/nagios/var/dcrmc_one.log
```


# Nagios Configuration

You can use the component with both event handler and obsessive compulsive nagios functionality, next you can see some configuration examples.


## Nagios obsessive compulsive

Nagios global configuration using obsessive compulsive in services, nagios.conf:

```
# OBSESS OVER SERVICE CHECKS OPTION
# This determines whether or not Nagios will obsess over service
# checks and run the ocsp_command defined below.  Unless you're
# planning on implementing distributed monitoring, do not enable
# this option.  Read the HTML docs for more information on
# implementing distributed monitoring.
# Values: 1 = obsess over services, 0 = do not obsess (default)

obsess_over_services=1


# OBSESSIVE COMPULSIVE SERVICE PROCESSOR COMMAND
# This is the command that is run for every service check that is
# processed by Nagios.  This command is executed only if the
# obsess_over_services option (above) is set to 1.  The command
# argument is the short name of a command definition that you
# define in your host configuration file. Read the HTML docs for
# more information on implementing distributed monitoring.

ocsp_command=opennebula!/usr/local/nagios/etc/config_one.xml
```


Nagios service configuration example using obsessive compulsive; fixed1.cfg:

```
define service{
        use      		   local-service        
        host_name                  fixed1
        service_description        HTTP
        check_command              check_http!3!8
        obsess_over_service        1
}
```



## Nagios event handler

Nagios command configuration to use the component dcrmc with event handler functionality; event-handlers.cfg:

```
define command {
   command_name    opennebula
   command_line $USER1$/dcrmc_one.pl $SERVICESTATE$ $SERVICESTATETYPE$ $SERVICEDESC$ $SERVICEATTEMPT$ $HOSTADDRESS$ $HOSTSTATE$ $ARG1$
}
```



Nagios service configuration example using event handler; fixed1.cfg:

```
define service{
   use  local-service ; Name of service template to use
   host_name             fixed1
   service_description   Total_Processes
   check_command         check_nrpe!check_total_procs!100!200!RSZDT
   active_checks_enabled      1
   event_handler  opennebula!/usr/local/nagios/etc/config_one.xml
}
```





