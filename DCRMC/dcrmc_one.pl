#!/usr/bin/perl
#
# File: dcrmc_one.pl
# Arguments: SERVICESTATE SERVICESTATETYPE SERVICEDESC SERVICEATTEMPT HOSTADDRESS HOSTSTATE XMLConfigFile
#
# ------------------------------------------------------------------------------------- #
#
# DESCRIPTION: 
#
# DCRMC (Dynamic Cloud Resource Management Component)
#
# Dynamic Cloud Resource Management Component that collect the value and params received by Nagios and config file to invoke the 
# algorithm that decides with next methods what kind of action make:
# 
# - getVMInfo: Collect all the information of a virtual machine.
# - setVMAction: Execute an action on a virtual machine.
# - getVMPoolInfo: Collect all the information of a virtual machine group.
# - getHOSTPoolInfo: Collect all the information of a cluster host.
# - getVMPoolFilterInfo: Collect all the information of a virtual machine filtered using params like ID, IP or state.
# - getVMPoolFilterInfoIP: Collect the virtual machine ID with a consult filtered with the virtual machine IP.
# - setVMAllocate: Create a new virtual machine with the predefined template.
# - setVMDeploy: Deploy a virtual machine.
# - getHOSTPoolFilterInfo: Collect all the information of a host filtered using params like ID.
# - SearchSuspendedAndAllocate: Search virtual machines with suspended state, if not exists create a new virtual machine ready to deploy.
# - SearchSuspendedAndDeploy: Search virtual machines with suspended state, if exists deploy the virtual machine, if not exists, create and deploy a new virtual machine.
#
#
# ------------------------------------------------------------------------------------- #
# Author: 
#    Miguel Morillo Iruela @m_morillo
#
# Directors: 
#    María Blanca Caminero Herráez mariablanca.caminero@uclm.es
#    María Carmen Carrión Espinosa Carmen.Carrion@uclm.es
#
# Trabajo Fin de Grado: Gestion dinamica de recursos en sistemas Cloud
# Escuela Superior de IngenieríInformáca https://esiiab.uclm.es/
# UCLM http://www.uclm.es/
# July 20th 2012
# ------------------------------------------------------------------------------------- #

use warnings;
use strict;
use Digest::SHA1  qw(sha1 sha1_hex sha1_base64);
use Frontier::Client;
use XML::Simple;
use XML::XPath;
use XML::XPath::XMLParser;
use Switch;
use Time::localtime;


my $num_args = $#ARGV;
if ($num_args != 6) {
  print "\nUsage: dcrmc_one.pl SERVICESTATE SERVICESTATETYPE SERVICEDESC SERVICEATTEMPT HOSTADDRESS HOSTSTATE XMLConfigFile\n";
  exit;
}

# -------------------------------------------------------------------------- #
# --------------  SETUP INITIALIZATION PARAMETERS  ------------------------- #
# -------------------------------------------------------------------------- #

my $path="/usr/local/nagios/";
my $log="$path/var/dcrmc_one.log";

open LOG, ">>$log"||die "ERROR: is not possible to write in the file $log\n";


# -------------------------------------------------------------------------- #
# --------------             ARGUMENTS             ------------------------- #
# -------------------------------------------------------------------------- #

my $SERVICESTATE=$ARGV[0];
my $SERVICESTATETYPE=$ARGV[1];
my $SERVICEDESC=$ARGV[2];
my $SERVICEATTEMPT=$ARGV[3];
my $HOSTADDRESS=$ARGV[4];
my $HOSTSTATE=$ARGV[5];
my $CONFIG = $ARGV[6];

my $conf = XMLin("$CONFIG");

# -------------------------------------------------------------------------- #
# --------------       CONFIG FILE PARAMETERS      ------------------------- #
# -------------------------------------------------------------------------- #

my $ONE=$conf->{ONE};                           # ONE
my $oneuser=$conf->{oneuser};                   # User
my $onepass=$conf->{onepass};                   # Pass
my $MAXVM=$conf->{maxVM};                       # Max number VM to deploy
my $TEMPLATEVM=$conf->{templateVM};             # MV Template
my $IP1=$conf->{VM1}->{IP};             		# MV1 IP
my $ESTADO1=$conf->{VM1}->{STATE};             	# MV1 STATE
my $IP2=$conf->{VM2}->{IP};             		# MV2 IP
my $ESTADO2=$conf->{VM2}->{STATE};             	# MV2 STATE
my $IP3=$conf->{VM3}->{IP};             		# MV3 IP
my $ESTADO3=$conf->{VM3}->{STATE};             	# MV3 STATE


# ------------------------------------------------------------------------ #
# --------------               VAR               ------------------------- #
# ------------------------------------------------------------------------ #

my $IP=$HOSTADDRESS;
my $ESTADO="";
my $IDVMSUSPEND="";
my $errorcode;
my $XML="";
my $server= Frontier::Client->new(url => $ONE);
##$password = sha1_hex($password); # Opennebula < 3.0
my $one_auth = "$oneuser:$onepass";
my $ID="";
my @vmpoolactive;
my @ipssuspendedstate;


# ------------------------------------------------------------------------- #
# ----------------------           MAIN        ---------------------------- #
# ------------------------------------------------------------------------- #


my %VMN= ( $IP1, $ESTADO1, $IP2, $ESTADO2, $IP3, $ESTADO3 );
# Detect VM STATE and IP that sent the event
while ( my ($key, $value) = each(%VMN) ) {
	#print LOG "$key => $value - $IP\n";
	if ($IP eq $key){
		$ESTADO=$value;
	}
}


print LOG timestamp() ."------------------------------------------------------------------------------------------\n";
print LOG timestamp() ."Received Nagios event: [SERVICESTATE: ",$SERVICESTATE,"] [SERVICESTATETYPE: ",$SERVICESTATETYPE,"]\n";
print LOG timestamp() ."Received Nagios event: [SERVICEATTEMPT: ",$SERVICEATTEMPT,"] [SERVICEDESC: ",$SERVICEDESC,"]\n";
print LOG timestamp() ."Received Nagios event: [HOSTADDRESS: ",$HOSTADDRESS,"] [HOSTSTATE: ",$HOSTSTATE,"]\n";
print LOG timestamp() ."OpenNebula address: [ONE: ",$ONE,"] [oneuser: ",$oneuser,"] [onepass: ",$onepass,"]\n";
print LOG timestamp() ."Maximun virtual machines number allowed to deploy: ",$MAXVM,"\n";
print LOG timestamp() ."Virtual machines defined: [",$IP1,"/",$ESTADO1,"] [",$IP2,"/",$ESTADO2,"] [",$IP3,"/",$ESTADO3,"]\n";
print LOG timestamp() ."Event sent by the virtual machine: [IP: ",$IP,"] [STATE: ",$ESTADO,"] [ID: ",$ID,"]\n";
print LOG timestamp() ."[ACTIONS PERFORMED]\n";



# ID of VM that sent the event
@vmpoolactive = &getVMPoolInfo(-1,-1,-1,3); # VM in ACTIVE STATE

if ($vmpoolactive[0][1] ne "<VM_POOL></VM_POOL>"){
	$ID=&getVMPoolFilterInfoIP(@vmpoolactive, $IP); # Returns the ID of the VM (IP) extracted from the XML getVMPoolInfoIP
}





# -------------------------------------------------------------------------------#
# -------------------------       FUNTIONS        -------------------------------#
# -------------------------------------------------------------------------------#


sub timestamp {
  my $t = localtime;
  return sprintf( "[%02d-%02d-%04d_%02d:%02d:%02d] ",
                  $t->mday, $t->mon + 1, $t->year + 1900,
                  $t->hour, $t->min, $t->sec );
}


sub getVMInfo {
	my $id;
	($id) = @_;

	my @response = $server->call('one.vm.info', $one_auth, $id);

	foreach my $line (@response) {
			print $line->[1], "\n";
	}
}



sub setVMAction {
	my $id;
	my $action;
	($id,$action) = @_;


	my @response = $server->call('one.vm.action', $one_auth, $action, $id);

	foreach my $line (@response) {
		print LOG $line->[1], "\n";
	}
}



sub getVMPoolInfo {
	my $flag;
	my $start;
	my $end;
	my $filter;
	($flag,$start,$end,$filter) = @_;

	my @response = $server->call('one.vmpool.info', $one_auth, $flag,$start,$end,$filter);
	
	return @response;
}

sub getHOSTPoolInfo {
	my @response = $server->call('one.hostpool.info', $one_auth);
	return @response;
}


sub getVMPoolFilterInfo {

	my @vmpoolinfo;
	my $filter;
	my $vmpoolinfoparam;
	
	($vmpoolinfoparam,$filter) = @_;
	
	@vmpoolinfo =  @{$vmpoolinfoparam};

	
	if($vmpoolinfo[2] != 0){
			$errorcode = $vmpoolinfo[2];
	}
	$XML = $vmpoolinfo[1];

	# Clean XML
	$XML =~ s/\"/\'/g;
	$XML =~ s/<!\[CDATA\[//g;
	$XML =~ s/\]\]>//g;

	my $xp = XML::XPath->new($XML);

	my $nodeset = $xp->find("//$filter/text()");
	my @infofilter;

	foreach my $node ($nodeset->get_nodelist) {
		push(@infofilter, XML::XPath::XMLParser::as_string($node));
	}

	return @infofilter;

}




sub getVMPoolFilterInfoIP {

	my @vmpoolinfo;
	my $filter;
	my $vmpoolinfoparam;
	
	($vmpoolinfoparam,$filter) = @_;
	
	@vmpoolinfo =  @{$vmpoolinfoparam};
	
	
	if($vmpoolinfo[2] != 0){
			$errorcode = $vmpoolinfo[2];
	}
	$XML = $vmpoolinfo[1];

	# Clean XML
	$XML =~ s/\"/\'/g;
	$XML =~ s/<!\[CDATA\[//g;
	$XML =~ s/\]\]>//g;

	my $xp = XML::XPath->new($XML);

	my $nodeset = $xp->find("//IP[text()=\"$filter\"]/../../../ID/text()");

	my @infofilter;

	foreach my $node ($nodeset->get_nodelist) {
		push(@infofilter, XML::XPath::XMLParser::as_string($node));
	}

							
	return $infofilter[0];
}


sub setVMAllocate{
    my $templatevm;
    ($templatevm) = @_;
        
    my @response = $server->call('one.vm.allocate', $one_auth, $templatevm);

    foreach my $line (@response) {
        print LOG timestamp() . $line->[1], "\n";
    }
}

sub setVMDeploy(){
    my $idvm;
    my $idhost;

    ($idvm,$idhost) = @_;

    my @response = $server->call('one.vm.deploy', $one_auth, $idvm, $idhost);

    foreach my $line (@response) {
		if($line->[2] == 0){
			$errorcode = $line->[2];
			print LOG timestamp() ."Error Deploying VM:",$errorcode, "\n";
		}
		else{
			print LOG timestamp() ." VM deployed correctly with ID:",$line->[1], "\n";
		}
	}


}




sub getHOSTPoolFilterInfo {

    my @hostpoolinfo;
    my $filter;
	my $hostpoolinfoparam;
    ($hostpoolinfoparam,$filter) = @_;
        

	@hostpoolinfo =  @{$hostpoolinfoparam};
	
	if($hostpoolinfo[2] != 0){
			$errorcode = $hostpoolinfo[2];
	}
	$XML = $hostpoolinfo[1];

	# Clean XML
	$XML =~ s/\"/\'/g;
	$XML =~ s/<!\[CDATA\[//g;
	$XML =~ s/\]\]>//g;

	my $xp = XML::XPath->new($XML);

	my $nodeset = $xp->find("//$filter/text()");
	my @infofilter;

	foreach my $node ($nodeset->get_nodelist) {
		push(@infofilter, XML::XPath::XMLParser::as_string($node));
	}

	return @infofilter;

}



sub SearchSuspendedAndAllocate{

my $f_SERVICESTATE;
my $f_ESTADO;

($f_SERVICESTATE,$f_ESTADO) = @_;

	print LOG timestamp() ."VM ",$f_ESTADO,": ",$f_SERVICESTATE," Search VM in suspend state, if exist, no action is required\n";
	my @vmpoolsuspended = &getVMPoolInfo(-1,-1,-1,5); # Informacion de maquinas en estado SUSPENDED
	my @ipssuspendedstate = &getVMPoolFilterInfo(@vmpoolsuspended ,"ID"); # Devuelve la informacion extraida del XML de getVMPoolInfo

	print LOG timestamp() ."VM ",$f_ESTADO,": ",$f_SERVICESTATE," Search VM in stopped state, if exist  --> vm.deploy\n";
	my @vmpoolstopped = &getVMPoolInfo(-1,-1,-1,4); # Informacion de maquinas en estado STOPPED
	my @idsstoppedstate = &getVMPoolFilterInfo(@vmpoolstopped ,"ID"); 
	
	if (@ipssuspendedstate){
		print LOG timestamp() ."VM ",$f_ESTADO,": ",$f_SERVICESTATE,", Exist VM in SUSPENDED state, waiting to resume \n";
	}
	elsif (@idsstoppedstate){
		print LOG timestamp() ."VM ",$f_ESTADO,": ",$f_SERVICESTATE,", Exist VM in STOPPED state, waiting to resume \n";
	}
	else{
		print LOG timestamp() ."VM ",$f_ESTADO,": ",$f_SERVICESTATE," no VM in SUSPENDED state\n";
		my @vmpoolany = &getVMPoolInfo(-1,-1,-1,-1); # Informacion de maquinas en estado Any state, except DONE
		my @ipsany = &getVMPoolFilterInfo(@vmpoolany ,"IP"); # Devuelve la informacion extraida del XML de getVMPoolInfo, en nuestro caso las IPs
		my $numberipsactive = scalar(grep {defined $_} @ipsany);
		if ($numberipsactive<$MAXVM){
			print LOG timestamp() ."VM ",$f_ESTADO,": ",$f_SERVICESTATE," Deploy VM --> vm.allocate\n";
			my @hostpoolany = &getHOSTPoolInfo();
			my @idhostsany = &getHOSTPoolFilterInfo(@hostpoolany ,"ID"); # Devuelve la informacion extraida del XML de getHOSTPoolFilterInfo, en nuestro caso las IDs
			my $IDHOST = $idhostsany[0];
			my $IDALLOCATE = setVMAllocate($TEMPLATEVM);
		}
		else{
			print LOG timestamp() ."VM ",$f_ESTADO,": ",$f_SERVICESTATE,", no deploy VM, maximum number reached $numberipsactive\n";
		}
	}
}





sub SearchSuspendedAndDeploy{

my $f_SERVICESTATE;
my $f_ESTADO;

($f_SERVICESTATE,$f_ESTADO) = @_;

	print LOG timestamp() ."VM ",$f_ESTADO,": ",$f_SERVICESTATE," Searching VM in init state, if exist  --> We will wait the stating of VMs \n";
	my @vmpoolinit = &getVMPoolInfo(-1,-1,-1,0); # Informacion de maquinas en estado INIT
	my @idsinitstate = &getVMPoolFilterInfo(@vmpoolinit ,"ID");
	
	print LOG timestamp() ."VM ",$f_ESTADO,": ",$f_SERVICESTATE," Searching VM in pending state, if exist  --> We will deploy the VMs \n";
	my @vmpoolpending = &getVMPoolInfo(-1,-1,-1,1); # Informacion de maquinas en estado PENDING
	my @idspendingstate = &getVMPoolFilterInfo(@vmpoolpending ,"ID"); 
	
	print LOG timestamp() ."VM ",$f_ESTADO,": ",$f_SERVICESTATE," Searching VM in hold state, if exist  --> We will wait the stating of VMs \n";
	my @vmpoolhold = &getVMPoolInfo(-1,-1,-1,2); # Informacion de maquinas en estado HOLD
	my @idsholdstate = &getVMPoolFilterInfo(@vmpoolhold ,"ID");
	
	print LOG timestamp() ."VM ",$f_ESTADO,": ",$f_SERVICESTATE," Searching VM in suspend state, if exist  --> vm.deploy\n";
	my @vmpoolsuspended = &getVMPoolInfo(-1,-1,-1,5); # Informacion de maquinas en estado SUSPENDED
	my @idssuspendedstate = &getVMPoolFilterInfo(@vmpoolsuspended ,"ID");
	
	print LOG timestamp() ."VM ",$f_ESTADO,": ",$f_SERVICESTATE," Searching VM in stopped state, if exist  --> vm.deploy\n";
	my @vmpoolstopped = &getVMPoolInfo(-1,-1,-1,4); # Informacion de maquinas en estado STOPPED
	my @idsstoppedstate = &getVMPoolFilterInfo(@vmpoolstopped ,"ID"); 

	
	if (@idsinitstate){
		print LOG timestamp() ."VM ",$f_ESTADO,": ",$f_SERVICESTATE,", VM in INIT state --> We will wait the stating of VMs \n";
	}
	elsif (@idspendingstate){
		print LOG timestamp() ."VM ",$f_ESTADO,": ",$f_SERVICESTATE,", VM in PENDING state --> We will deploy VMs \n";
		my $IDVMPENDING = $idspendingstate[0]; # Cogemos el primer valor del array de IDs
		my @hostpoolany = &getHOSTPoolInfo();
        my @idhostsany = &getHOSTPoolFilterInfo(@hostpoolany ,"ID"); # Devuelve la informacion extraida del XML de getHOSTPoolFilterInfo, en nuestro caso las IDs
		my $IDHOST = $idhostsany[0];
        &setVMDeploy($IDVMPENDING, $IDHOST);
	}
	elsif (@idsholdstate){
		print LOG timestamp() ."VM ",$f_ESTADO,": ",$f_SERVICESTATE,", VM in HOLD state --> We will wait the stating of VMs \n";
	}
	elsif (@idssuspendedstate){
		print LOG timestamp() ."VM ",$f_ESTADO,": ",$f_SERVICESTATE,", VM in SUSPENDED state --> Deploy\n";
		my $IDVMSUSPEND = $idssuspendedstate[0]; # Cogemos el primer valor del array de IDs
		&setVMAction($IDVMSUSPEND,'resume');
	}
	elsif (@idsstoppedstate){
		print LOG timestamp() ."VM ",$f_ESTADO,": ",$f_SERVICESTATE,", VM in STOPPED state --> Deploy\n";
		my $IDVMSTOPPED = $idsstoppedstate[0]; # Cogemos el primer valor del array de IDs
		&setVMAction($IDVMSTOPPED,'resume');
	}
	else{
		print LOG timestamp() ."VM ",$f_ESTADO,": ",$f_SERVICESTATE,", no VM in SUSPENDED or STOPPED state\n";
		my @vmpoolany = &getVMPoolInfo(-1,-1,-1,3); # Informacion de maquinas en estado ACTIVE
		my @ipsany = &getVMPoolFilterInfo(@vmpoolany ,"IP"); # Devuelve la informacion extraida del XML de getVMPoolInfo, en nuestro caso las IPs
		my $numberipsactive = scalar(grep {defined $_} @ipsany);
		if ($numberipsactive<$MAXVM){
			print LOG timestamp() ."VM ",$f_ESTADO,": ",$f_SERVICESTATE," Allocate VM --> vm.allocate\n";
			my @hostpoolany = &getHOSTPoolInfo();
			my @idhostsany = &getHOSTPoolFilterInfo(@hostpoolany ,"ID"); # Devuelve la informacion extraida del XML de getHOSTPoolFilterInfo, en nuestro caso las IDs
			my $IDHOST = $idhostsany[0];
			my $IDALLOCATE = setVMAllocate($TEMPLATEVM);
			# pause? sleep(seconds); o if boolean devuelte = true{}
			&setVMDeploy($IDALLOCATE, $IDHOST);
		}
		else{
			print LOG timestamp() ."VM ",$f_ESTADO,": ",$f_SERVICESTATE,", no deploy VM, maximum number reached\n";
		}
	}
}


# ---------------------------------------------------------------------------------------#
# ---------------------------       ALGORITHM       -------------------------------------#
# ---------------------------------------------------------------------------------------#


switch ($SERVICESTATE) {
	case "OK" {
		if ($ESTADO eq "FIXED") {
			print LOG timestamp() ."VM FIXED: OK\n";
		}
		elsif ($ESTADO eq "RESERVED" and $HOSTSTATE eq "UP") {
			print LOG timestamp() ."VM RESERVED: OK --> Shutdown VM\n";
			#&setVMAction($ID,'shutdown');
			#&setVMAction($ID,'stopped');
			&setVMAction($ID,'suspend');
		}
	}
	case "WARNING" {
		switch ($SERVICESTATETYPE){
			case "SOFT" {
				switch ($SERVICEATTEMPT){
					case "3" {
						if ($ESTADO eq "FIXED") {
							&SearchSuspendedAndAllocate($SERVICESTATE ,$ESTADO);			
						} # FIXED
						elsif ($ESTADO eq "RESERVED" and $HOSTSTATE eq "UP") {
							print LOG timestamp() ."VM RESERVED: WARNING, Suspend VM --> vm.suspend\n";
							&setVMAction($ID,'suspend');
						} # RESERVED
					} # Case 3
				}
			} # SOFT
			case "HARD" {
				if ($ESTADO eq "FIXED") {
					&SearchSuspendedAndDeploy($SERVICESTATE ,$ESTADO);		
				} # FIXED
				elsif ($ESTADO eq "RESERVED" and $HOSTSTATE eq "UP") {
					&SearchSuspendedAndDeploy($SERVICESTATE ,$ESTADO);
				} # RESERVED
			} # HARD
		}
	} # WARNING
	case "UNKNOWN" {
	}
	case "CRITICAL" {
		if ($ESTADO eq "FIXED") {
			&SearchSuspendedAndDeploy($SERVICESTATE ,$ESTADO);
		} # FIXED
		elsif ($ESTADO eq "RESERVED" and $HOSTSTATE eq "UP") {
			&SearchSuspendedAndDeploy($SERVICESTATE ,$ESTADO);
		} # RESERVED											
	} # CRITICAL
	else {
			print LOG timestamp() ."Default ------------\n";
	}
}




# ---------------------------------------------------------------------------------------#
# ---------------------------          END           ------------------------------------#
# ---------------------------------------------------------------------------------------#



close LOG;


